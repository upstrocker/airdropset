import React, { Component, Fragment } from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router";
import Donation from './containers/pages/Donation/Donation';
import Listing from "./containers/pages/Listing/Listing";
import About from "./containers/pages/About/About";
import Login from "./containers/pages/Login/Login";
import Blog from "./containers/pages/Blog/Blog";
import Search from "./containers/pages/Search/Search";
import Home from "./containers/pages/Home/Home";
import Airdrop from "./containers/pages/Airdrop/Airdrop";
import Airdrops from "./containers/pages/Airdrops/Airdrops";
import {
  ABOUT_URL, AIRDROP_ITEM_URL,
  AIRDROP_URL,
  BLOG_URL,
  DONATION_URL,
  HOME_URL,
  LISTING_URL,
  LOGIN_URL,
  SEARCH_URL
} from "./constants";

class App extends Component {
  render() {
    const routes = (
      <Switch>
        <Route path={DONATION_URL} component={Donation} />
        <Route path={LISTING_URL} component={Listing} />
        <Route path={ABOUT_URL} component={About} />
        <Route path={LOGIN_URL} component={Login} />
        <Route path={BLOG_URL} component={Blog} />
        <Route path={SEARCH_URL} component={Search} />
        <Route path={AIRDROP_ITEM_URL} component={Airdrop} />
        <Route path={AIRDROP_URL} component={Airdrops} />
        <Route path={HOME_URL} exact component={Home} />
        <Redirect to={HOME_URL} />
      </Switch>
    );
    return (
      <Fragment>
        {routes}
      </Fragment>
    );
  }
}

export default withRouter(App);
