import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Header from "../../../components/Header/Header";
import CoinBar from "../../../components/CoinBar/CoinBar";
import NewsBar from "../../../components/NewsBar/NewsBar";
import AirdropList from "../../../components/AirdropList/AirdropList";
import './Home.scss';
import SignUp from "../../../components/SignUp/SignUp";
import Faq from "../../../components/Faq/Faq";
import Exchanges from "../../../components/Exchanges/Exchanges";
import Footer from "../../../components/Footer/Footer";

class Home extends Component {

  render() {
    return (
      <Fragment>
        <Header />
        <main>
          <AirdropList />
          <SignUp />
          <Faq />
          <Exchanges />
        </main>
        <Footer />
      </Fragment>
    )
  }
}

export default Home;