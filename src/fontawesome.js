// import the library
import { library } from '@fortawesome/fontawesome-svg-core';

// import your icons
import {fab, faFacebook, faTelegram, faTwitter, faInstagram, faVk} from '@fortawesome/free-brands-svg-icons';
import {faSearch, faCaretDown, faCaretUp, faCheck,
  faUser, faUsers, faUserFriends, faHourglass, faWallet, faStar,
  faNewspaper, faCalendarCheck, faPaperPlane, faGem, faGift, faAngleDown, faHeart,
  faBars, faSearchPlus, faThumbsUp, faThumbsDown, faChevronDown, faChevronUp
} from '@fortawesome/free-solid-svg-icons';

library.add(fab, faFacebook, faTelegram, faTwitter, faInstagram, faVk,
  faSearch, faCaretDown, faCaretUp, faCheck, faUser, faUsers, faUserFriends, faHourglass, faWallet, faStar,
  faNewspaper, faCalendarCheck, faPaperPlane, faGem, faGift, faAngleDown, faHeart, faBars, faSearchPlus,
  faThumbsUp, faThumbsDown, faChevronDown, faChevronUp);