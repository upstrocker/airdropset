import React from 'react';
import './Exchanges.scss';

const Exchanges = () => {
  return (
    <section className="exchanges" id="exchanges">
      <h2>Exchanges</h2>
      <div className="exchanges-list">
        <a className="exchange" href="https://binance.com"><img src="images/binance.png" alt="Binance" /></a>
        <a className="exchange" href="https://kucoin.com"><img src="images/kucoin.png" alt="Kucoin" /></a>
        <a className="exchange" href="https://cobinhood.com"><img src="images/cobinhood.png" alt="Cobinhood" /></a>
        <a className="exchange" href="https://cryptopia.com"><img src="images/cryptopia.png" alt="Cryptopia" /></a>
        <a className="exchange" href="https://coinexchange.io"><img src="images/coinexchange.png" alt="Coinexchange" /></a>
        <a className="exchange" href="https://hitbtc.com"><img src="images/hitbtc.png" alt="Hitbtc" /></a>
        <a className="exchange" href="https://huobi.com"><img src="images/huobi.png" alt="Huobi" /></a>
        <a className="exchange" href="https://bit-z.com"><img src="images/bitz.png" alt="Bit-Z" /></a>
      </div>
    </section>
  )
};

export default Exchanges;