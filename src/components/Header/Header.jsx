import React, {Component, Fragment} from 'react';
import './Header.scss';
import {Link} from "react-router-dom";
import Menu from "../Menu/Menu";
import SearchBox from "../SearchBox/SearchBox";
import SocialMenu from "../SocialMenu/SocialMenu";
import {ABOUT_URL, BLOG_URL, DONATION_URL, HEADER_LOGO_SRC, HOME_URL, LISTING_URL, LOGIN_URL} from "../../constants";
import CoinBar from "../CoinBar/CoinBar";
import NewsBar from "../NewsBar/NewsBar";
import * as axios  from 'axios';

const headerLinks = [
  {url: HOME_URL, text: 'Home'},
  {url: ABOUT_URL, text: 'About'},
  {url: LISTING_URL, text: 'Listing'},
  {url: BLOG_URL, text: 'Blog'},
  {url: DONATION_URL, text: 'Support Us'},
  {url: LOGIN_URL, text: 'Log In'},
];

class Header extends Component {

  state = {
    coins: []
  };

  componentDidMount() {
    fetch('https://api.binance.com/api/v3/ticker/price')
      .then(function (response) {

        console.log(response.json());
      })
      .catch(function (error) {
        console.log('Request failed', error)
      });
  }

  render() {

    return (
      <Fragment>
        <header>
          <h1 className="logo">
            <Link to={HOME_URL}><img src={HEADER_LOGO_SRC} alt="AirdropSet" /></Link>
          </h1>
          <Menu id="main-menu" links={headerLinks} hasMobile />
          <SearchBox />
          <SocialMenu />
        </header>
        <CoinBar />
        <NewsBar />
      </Fragment>
    )
  }
}

export default Header;

