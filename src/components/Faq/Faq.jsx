import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import './Faq.scss';

const Faq = () => {
  return (
    <section className="faq" id="faq">
      <h2>Faq</h2>
      <div className="faq-item">
        <h3 className="question">
          What's airdrop means?
          <FontAwesomeIcon className="toggle-answer" icon="chevron-down" />
        </h3>
        <p className="answer">TokenDrops is NOT distributing tokens. You can check completed airdrops'
          ICO ending dates. Projects will send the airdrop tokens to your Ethereum address in 3 months
          after their ICO ends. You will see "Distributed" tag on completed airdrops after the distribution process.</p>
      </div>
      <div className="faq-item">
        <h3 className="question">
          What's airdrop means?
          <FontAwesomeIcon className="toggle-answer" icon="chevron-down" />
        </h3>
        <p className="answer">TokenDrops is NOT distributing tokens. You can check completed airdrops'
          ICO ending dates. Projects will send the airdrop tokens to your Ethereum address in 3 months
          after their ICO ends. You will see "Distributed" tag on completed airdrops after the distribution process.</p>
      </div>
      <div className="faq-item">
        <h3 className="question">
          What's airdrop means?
          <FontAwesomeIcon className="toggle-answer" icon="chevron-down" />
        </h3>
        <p className="answer">TokenDrops is NOT distributing tokens. You can check completed airdrops'
          ICO ending dates. Projects will send the airdrop tokens to your Ethereum address in 3 months
          after their ICO ends. You will see "Distributed" tag on completed airdrops after the distribution process.</p>
      </div>
      <div className="faq-item">
        <h3 className="question">
          What's airdrop means?
          <FontAwesomeIcon className="toggle-answer" icon="chevron-down" />
        </h3>
        <p className="answer">TokenDrops is NOT distributing tokens. You can check completed airdrops'
          ICO ending dates. Projects will send the airdrop tokens to your Ethereum address in 3 months
          after their ICO ends. You will see "Distributed" tag on completed airdrops after the distribution process.</p>
      </div>
      <div className="faq-item">
        <h3 className="question">
          What's airdrop means?
          <FontAwesomeIcon className="toggle-answer" icon="chevron-down" />
        </h3>
        <p className="answer">TokenDrops is NOT distributing tokens. You can check completed airdrops'
          ICO ending dates. Projects will send the airdrop tokens to your Ethereum address in 3 months
          after their ICO ends. You will see "Distributed" tag on completed airdrops after the distribution process.</p>
      </div>
      <div className="faq-item">
        <h3 className="question">
          What's airdrop means?
          <FontAwesomeIcon className="toggle-answer" icon="chevron-down" />
        </h3>
        <p className="answer">TokenDrops is NOT distributing tokens. You can check completed airdrops'
          ICO ending dates. Projects will send the airdrop tokens to your Ethereum address in 3 months
          after their ICO ends. You will see "Distributed" tag on completed airdrops after the distribution process.</p>
      </div>
    </section>
  )
};

export default Faq;