import React from 'react';
import './SignUp.scss';
import {Button, Input} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

const SignUp = () => {
  return (
    <section className="sign-up">
      <h2>Sign up and control your drops</h2>
      <form className="sign-up-form">
        <TextField
          id="sing-up__username"
          className="sign-up-input"
          type="text"
          label="Username"
          placeholder="Username"
        />
        <TextField
          id="sign-up__email"
          className="sign-up-input"
          type="email"
          label="Email"
          placeholder="Email"
          required
        />
        <TextField
          id="sign-up__password"
          className="sign-up-input"
          type="password"
          label="Password"
          placeholder="Password"
          required
        />
        <div className="sign-up-buttons">
          <Button id="sign-up-button" type="submit" variant="raised">Sign Up</Button>
          <Button id="log-in-button" type="submit" variant="flat">or Log In</Button>
        </div>
        <p className="agreement">By joining AirdropSet, you agree to our <a href="terms_of_service.html">Terms of
          Service</a> and <a href="privacy_policy.html">Privacy Policy</a></p>
        <div className="social-login-group">
          <div className="social-login">
            <div className="social-login-button">
              <i className="fa fa-facebook-square"/> Join with Facebook
            </div>
          </div>
          <div className="social-login">
            <div className="social-login-button">
              <i className="fa fa-twitter-square"/> Join with Twitter
            </div>
          </div>
          <div className="social-login">
            <div className="social-login-button">
              <i className="fa fa-instagram"/> Join with Instagram
            </div>
          </div>
          <div className="social-login">
            <div className="social-login-button">
              <i className="fa fa-google-plus-square"/> Join with Google
            </div>
          </div>
        </div>
      </form>
    </section>
  )
};

export default SignUp;
