import React from 'react';
import './NewsBar.scss';

const NewsBar = () => {

  return (
    <section className="news-bar">
      <div className="news-item">
        <span className="news-title">Top 5 daily drops</span>
        <span className="news-date">(01-11)</span>
      </div>
      <div className="news-item">
        <span className="news-title">Top 5 daily drops</span>
        <span className="news-date">(01-11)</span>
      </div>
      <div className="news-item">
        <span className="news-title">Top 5 daily drops</span>
        <span className="news-date">(01-11)</span>
      </div>
    </section>
  )
};

export default NewsBar;