import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import './AirdropItem.scss';

const AirdropItem = ({link, logoSrc, name, ticker, isExclusive, isActive, isSelected, likes, dislikes}) => {
  return (
    <li className="airdrop-item">
      <a href={link}>
        <div className="airdrop-logo"><img src={logoSrc} alt={name} /></div>
        <h2>{`${name} (${ticker})`}</h2>
        <div className="additional-info">
          <div className="exclusive-info">
            {isExclusive ? <span className="exclusive">Exclusive</span> : null}
            {isSelected ? <FontAwesomeIcon icon={'check'} /> : null}
          </div>
          <div className="rating">
            <span className="likes">{likes} <FontAwesomeIcon icon={'thumbs-up'} /></span>
            <span className="dislikes">{dislikes} <FontAwesomeIcon icon={'thumbs-down'} /></span>
          </div>
        </div>
      </a>
    </li>
  )
};

export default AirdropItem;