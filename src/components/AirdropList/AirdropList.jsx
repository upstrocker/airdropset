import React from 'react';
import AirdropItem from "../AirdropItem/AirdropItem";
import './AirdropList.scss';

const airdrops = [
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
  {link: '/airdrops/bcpt', logoSrc: 'images/logo-airdrop.png', name: 'BlockMason Credit Network', ticker: 'BCPT', isExclusive: true, isActive: true, isSelected: true, likes: 100, dislikes: 100},
];

const AirdropList = () => {
  return (
    <section className="airdrop-list">
      <form className="filter">
        <select name="sorting" id="sorting" title="Sorting Field">
          <option value="name">Sorting By Name</option>
          <option value="date" selected>Sorting By Date</option>
        </select>
        <label>
          <input className="only-exclusive" type="checkbox" />
            Only Exclusive
        </label>
      </form>
      <ul className="airdrops">
        {airdrops.map((airdrop, index) => {
          console.log(airdrop);
          return (
          <AirdropItem key={`${airdrop.name}-${airdrop.ticker}`} {...airdrop} />)
        })}
      </ul>
      <nav className="pagination">
        <a href="" className="prev">&lt;</a>
        <a href="">1</a>
        <a href="">2</a>
        <a href="">3</a>
        <a href="" className="current">4</a>
        <a href="">5</a><a href="" className="next">&gt;</a>
      </nav>
    </section>
  )
};

export default AirdropList;