import React from 'react';
import './SocialMenu.scss';
import {FACEBOOK_LINK, INSTAGRAM_LINK, TELEGRAM_LINK, TWITTER_LINK} from "../../constants";
import SocialLink from "../SocialLink/SocialLink";

const SocialMenu = () => (
  <nav>
    <ul className="social-links">
      <li><SocialLink link={TELEGRAM_LINK} faIconName="telegram" /></li>
      <li><SocialLink link={TWITTER_LINK} faIconName="twitter" /></li>
      <li><SocialLink link={INSTAGRAM_LINK} faIconName="instagram" /></li>
      <li><SocialLink link={FACEBOOK_LINK} faIconName="facebook" /></li>
    </ul>
  </nav>
);

export default SocialMenu;