import React, {Fragment} from "react";
import './SocialLink.scss';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFacebook} from "@fortawesome/free-brands-svg-icons";

const SocialLink = ({link, faIconName, text}) => (
  <Fragment>
    <a href={link}>
      {faIconName ? <FontAwesomeIcon icon={['fab', faIconName]} /> : text}
    </a>
  </Fragment>
);

SocialLink.propTypes = {
  link: PropTypes.string,
  faIconName: PropTypes.string,
  text: PropTypes.string,
};

SocialLink.defaultProps = {
  link: '',
  faIconName: '',
  text: '',
};


export default SocialLink;