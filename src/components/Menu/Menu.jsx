import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Link} from "react-router-dom";
import './Menu.scss';
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";

class Menu extends Component {
  state = {
    top: false,
    left: false,
    bottom: false,
    right: false,
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    const {id, links, hasMobile} = this.props;
    const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
    const menu = (
      <ul className="links" id={id}>
      {links.map((link, index) => (
          <li><Link key={index} to={link.url}>{link.text}</Link></li>
        ))}
  </ul>
    );
    return (
      <nav>
        {menu}
        {
          hasMobile
            ?
            <Fragment>
              <FontAwesomeIcon onClick={this.toggleDrawer('left', true)} icon={'bars'} id="burger-menu"/>
              <SwipeableDrawer
                open={this.state.left}
                onClose={this.toggleDrawer('left', false)}
                onOpen={this.toggleDrawer('left', true)}
                disableBackdropTransition={!iOS} disableDiscovery={iOS}
              >
                <div
                  tabIndex={0}
                  role="button"
                  onClick={this.toggleDrawer('left', false)}
                  onKeyDown={this.toggleDrawer('left', false)}
                >
                  {
                    <div>
                      <List>
                        {links.map((link, index) => (
                          <ListItem button key={link.text}>
                            <Link to={link.url}><ListItemText primary={link.text} /> </Link>
                          </ListItem>
                        ))}
                      </List>
                    </div>
                  }
                </div>
              </SwipeableDrawer>
            </Fragment>
            : null
        }
      </nav>
    )
  }
}

Menu.propTypes = {
  id: PropTypes.string,
  links: PropTypes.array,
  hasMobile: PropTypes.bool
};

Menu.defaultProps = {
  id: '',
  links: [],
  hasMobile: false
};

export default Menu;