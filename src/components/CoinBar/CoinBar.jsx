import React from 'react';
import 'crypto-icons/font.css';
import 'crypto-icons/styles.css';
import './CoinBar.scss';

const CoinBar = () => {

  return (
    <section className="coin-bar">
      <div className="coin">
        <span className="icon icon-btc"/>
        <span className="coin-price"> 20000$</span>
      </div>
      <div className="coin">
        <span className="icon icon-btc up"/>
        <span className="coin-price"> 20000$</span>
      </div>
      <div className="coin">
        <span className="icon icon-btc"/>
        <span className="coin-price"> 20000$</span>
      </div>
      <div className="coin">
        <span className="icon icon-btc down"/>
        <span className="coin-price"> 20000$</span>
      </div>
      <div className="coin">
        <span className="icon icon-btc"/>
        <span className="coin-price"> 20000$</span>
      </div>
      <div className="coin">
        <span className="icon icon-btc"/>
        <span className="coin-price"> 20000$</span>
      </div>
      <div className="coin">
        <span className="icon icon-btc"/>
        <span className="coin-price"> 20000$</span>
      </div>
      <div className="coin">
        <span className="icon icon-btc"/>
        <span className="coin-price"> 20000$</span>
      </div>
    </section>
  )
};

export default CoinBar;