import React from 'react';
import './SearchBox.scss';
import Input from "@material-ui/core/Input";
import {withStyles} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';

const styles = theme => ({
  search: {
    position: 'relative',
    width: 'auto'
  },
  searchIcon: {
    position: 'absolute',
    cursor: 'pointer',
    color: '#fff',
    zIndex: '1'
  },
  inputRoot: {
    color: '#fff',
    width: '100%',
  },
  inputInput: {
    fontSize: '1.4rem',
    paddingLeft: theme.spacing.unit * 5,
  },
  underline: {
    '&:after': {
      // The MUI source seems to use this but it doesn't work
      borderBottom: '2px solid #fab915',
    },
  }
});

const SearchBox = ({classes}) => (
  <div className={classes.search}>
    <div className={classes.searchIcon}>
      <SearchIcon />
    </div>
    <Input
      placeholder="Search…"
      classes={{
        root: classes.inputRoot,
        input: classes.inputInput,
        underline: classes.underline
      }}
    />
  </div>
);

export default withStyles(styles)(SearchBox);