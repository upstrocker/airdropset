import React from 'react';
import './Footer.scss';
import {Link} from "react-router-dom";
import {
  ABOUT_URL,
  BLOG_URL,
  DONATION_URL, EXCHANGES_URL,
  FAQ_URL,
  FOOTER_LOGO_SRC,
  HOME_URL,
  LISTING_URL,
  PRIVACY_URL, TERMS_URL
} from "../../constants";
import Menu from "../Menu/Menu";
import SocialMenu from "../SocialMenu/SocialMenu";

const footerLinks = [
  {url: HOME_URL, text: 'Home'},
  {url: LISTING_URL, text: 'About'},
  {url: DONATION_URL, text: 'Support Us'},
  {url: BLOG_URL, text: 'Blog'},
  {url: FAQ_URL, text: 'Faq'},
];

const footerLinksTwo = [
  {url: TERMS_URL, text: 'Terms'},
  {url: PRIVACY_URL, text: 'Privacy'},
  {url: EXCHANGES_URL, text: 'Exchanges'},
  {url: ABOUT_URL, text: 'About'},
];

const Footer = () => (
  <footer>
    <div className="footer-navs">
      <div className="footer-navs-menu">
        <Menu links={footerLinks} />
        <Menu links={footerLinksTwo} />
      </div>
      <SocialMenu />
    </div>
    <div className="credentials">
      <div className="logo">
        <Link to={HOME_URL}><img src={FOOTER_LOGO_SRC} alt="AirdropSet footer logo" /></Link>
      </div>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis </p>
      <p className="copyright">airdropset.com &copy;2019</p>
    </div>
  </footer>
);

export default Footer;