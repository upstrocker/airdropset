// Action Constants
export const CHANGE_COINBAR = 'CHANGE_COINBAR';

// Links Constants
export const FACEBOOK_LINK = 'https://facebook.com/airdropset';
export const INSTAGRAM_LINK = 'https://instagram.com/airdropset';
export const TWITTER_LINK = 'https://twitter.com/airdropset';
export const TELEGRAM_LINK = 'https://telegram.com/airdropset';

export const HOME_URL = '/';
export const ABOUT_URL = '/about';
export const LISTING_URL = '/listing';
export const BLOG_URL = '/blog';
export const FAQ_URL = '/faq';
export const DONATION_URL = '/donation';
export const LOGIN_URL = '/login';
export const SEARCH_URL = '/search';
export const EXCHANGES_URL = '/#exchanges';
export const PRIVACY_URL = '/privacy-policy';
export const TERMS_URL = '/terms-of-service';
export const AIRDROP_URL = '/airdrop';
export const AIRDROP_ITEM_URL = AIRDROP_URL + '/:slug';


export const HEADER_LOGO_SRC = '/images/logo.png';
export const FOOTER_LOGO_SRC = '/images/logo-footer.png';
