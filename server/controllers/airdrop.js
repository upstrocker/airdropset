const Airdrop = require('../models/Airdrop');
const errorHandler = require('../utils/errorHandler');
// note: secure
module.exports.getAll = async function (req, res) {
  const query = {};

  if (req.query.onlyExclusive) {
    query.isExclusive = true;

  }
  const sort = req.query.sortDate ? {date: 1} : req.query.sortName ? {name: 1} : {};
  try {
    const airdrops = await Airdrop
      .find(query)
      .sort(sort)
      .skip(+req.query.offset)
      .limit(+req.query.limit);
    res.status(200).json(airdrops);
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.getById = async function (req, res) {
  try {
    const airdrop = await Airdrop.find({
      _id: req.params.id
    });
    res.status(200).json({airdrop});
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.getPublished = async function (req, res) {
  try {
    const airdrops = Airdrop.find({
      isPublished: true
    });
    res.status(200).json({airdrops});
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.getActive = async function (req, res) {
  try {
    const airdrops = Airdrop.find({
      isActive: true
    });
    res.status(200).json({airdrops});
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.getExclusive = async function (req, res) {
  try {
    const airdrops = Airdrop.find({
      isExclusive: true
    });
    res.status(200).json({airdrops});
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.getPage = async function (req, res) {
  try {

  } catch (e) {
    errorHandler(res, e);
  }
};

// note: secure
module.exports.remove = async function (req, res) {
  try {
    await Airdrop.remove({_id: req.params.id});
    res.status(200).json({
      message: 'Airdrop with id = ' + req.params.id + ' was removed!'
    })
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.create = async function (req, res) {
  const airdrop = new Airdrop({
    // name: req.body.name,
    // endDate: req.body.endDate,
    logoSrc: req.file ? req.file.path : '',
    // formLink: req.body.formLink,
    // amount: req.body.amount,
    // amountPerRef: req.body.amountPerRef,
    // ticker: req.body.ticker,
    // decimal: req.body.decimal,
    // tokenType: req.body.tokenType,
    // totalSupply: req.body.totalSupply,
    // pricePerToken: req.body.pricePerToken,
    // description: req.body.description,
    // companyName: req.body.companyName,
    // country: req.body.country,
    // website: req.body.website,
    // socialLinks: req.body.socialLinks,
    // whitepaperLink: req.body.whitepaperLink,
    // email: req.body.email,
    // bountyLink: req.body.bountyLink,
    // created: req.body.created,
    // isExclusive: req.body.isExclusive,
    // maxParticipants: req.body.maxParticipants,
    // rules: req.body.rules,
  });
  try {
    await airdrop.save();
    res.status(201).json(airdrop);
  } catch (e) {
    errorHandler(res, e);
  }
};

// note: secure
module.exports.update = async function (req, res) {
  const updated = {
    ...req.body
  };
  if (req.file) {
    updated.logoSrc = req.file.path;
  }
  try {
    const airdrop = await Airdrop.findOneAndUpdate(
      {_id: req.params.id},
      {$set: updated},
      {new: true}
    );
    res.status(200).json(airdrop);
  } catch (e) {
    errorHandler(res, e);
  }
};