const express = require('express');
const upload = require('../middleware/upload');
const controller = require('../controllers/airdrop');
const router = express.Router();

router.get('/', controller.getPublished);
router.get('/all', controller.getAll);
router.get('/active', controller.getActive);
router.get('/exclusive', controller.getExclusive);
router.get('/:pageId', controller.getPage);
router.get('/:id', controller.getById);
router.delete('/:id', controller.remove);
router.post('/', upload.single('logo'), controller.create);
router.patch('/:id', upload.single('logo'), controller.update);

module.exports = router;