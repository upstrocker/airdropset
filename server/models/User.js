const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true,
    unique: true
  },
  userCategory: {
    ref: 'userCategories',
    type: Schema.Types.ObjectId
  },
  avatarSrc: {
    type: String,
    default: ''
  },
  referalCode: {
    type: String,
    default: '' //TODO: autogen code
  },
  referrer: {
    type: String,
    default: ''
  },
  phone: {
    type: String,
    default: ''
  },
  airdrops: {
    ref: 'airdrops',
    type: Schema.Types.ObjectId
  }
});

module.exports = mongoose.model('users', userSchema);