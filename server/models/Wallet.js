const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const walletSchema = new Schema({
  blockchain: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  user: {
    ref: 'users',
    type: Schema.Types.ObjectId
  }
});

module.exports = mongoose.model('wallets', walletSchema);