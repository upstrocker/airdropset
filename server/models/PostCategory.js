const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postCategorySchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  posterSrc: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('postCategories', postCategorySchema);