const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userCategorySchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  permissions: [{
    ref: 'permissions',
    type: Schema.Types.ObjectId
  }]
});

module.exports = mongoose.model('userCategories', userCategorySchema);