const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  updated: {
    type: Date,
    default: Date.now
  },
  brief: {
    type: String,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  published: {
    type: Boolean,
    default: false
  },
  author: {
    ref: 'users',
    type: Schema.Types.ObjectId
  },
  posterSrc: {
    type: String,
    required: true,
  },
  category: {
    ref: 'postCategories',
    type: Schema.Types.ObjectId
  },
  tags: [{
    ref: 'tags',
    type: Schema.Types.ObjectId
  }]
});

module.exports = mongoose.model('posts', postSchema);