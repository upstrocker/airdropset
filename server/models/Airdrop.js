const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const airdropSchema = new Schema({
  // name: {
  //   type: String,
  //   required: true
  // },
  // endDate: {
  //   type: Date,
  //   required: true
  // },
  logoSrc: {
    type: String,
    default: ''
  },
  formLink: {
    type: String,
    default: ''
  },
  // amount: {
  //   type: Number,
  //   required: true
  // },
  amountPerRef: {
    type: Number,
    default: 0
  },
  // ticker: {
  //   type: String,
  //   required: true,
  // },
  // decimal: {
  //   type: String,
  //   required: true,
  // },
  // tokenType: {
  //   type: String,
  //   required: true
  // },
  // totalSupply: {
  //   type: Number,
  //   required: true
  // },
  // pricePerToken: {
  //   type: Number,
  //   required: true
  // },
  // description: {
  //   type: String,
  //   required: true,
  // },
  // companyName: {
  //   type: String,
  //   required: true,
  // },
  country: {
    type: String,
    default: ''
  },
  website: {
    type: String,
    default: ''
  },
  socialLinks: [{
    link: {
      type: String,
    }
  }],
  whitepaperLink: {
    type: String,
    default: ''
  },
  // email: {
  //   type: String,
  //   required: true
  // },
  bountyLink: {
    type: String,
    default: ''
  },
  created: {
    type: Date,
    default: Date.now
  },
  isPublished: {
    type: Boolean,
    default: false
  },
  likes: {
    type: Number,
    default: 0,
  },
  dislikes: {
    type: Number,
    default: 0
  },
  isExclusive: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  },
  maxParticipants: {
    type: Number,
    default: 0
  },
  rules: [{
    type: String,
    default: ''
  }]
});

module.exports = mongoose.model('airdrops', airdropSchema);